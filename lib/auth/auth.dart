import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:okfantasy_app/auth/login/screen/login.dart';
import 'package:okfantasy_app/auth/register/screen/register.dart';

class Auth extends StatefulWidget {
  @override
  _AuthState createState() => _AuthState();
}

class _AuthState extends State<Auth> {
  bool _isVisible = true;
  void changeForm() {
    setState(() {
      _isVisible = !_isVisible;
    });
  }

  Widget build(BuildContext context) {
    double _width = MediaQuery.of(context).size.width;
    double _height = MediaQuery.of(context).size.height;

    final newUser = MaterialButton(
      minWidth: _width * 0.9,
      height: _height * 0.06,
      elevation: 0,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
          side: BorderSide(color: Color.fromRGBO(0, 3, 42, 1))),
      color: Colors.transparent,
      onPressed: () => {changeForm()},
      child: Text(
        "Sign Up",
        style: GoogleFonts.nunito(
          fontSize: 20,
          fontWeight: FontWeight.w800,
          textStyle: TextStyle(color: Color.fromRGBO(0, 3, 42, 1)),
        ),
      ),
    );

    final haveAccount = MaterialButton(
      minWidth: _width * 0.9,
      height: _height * 0.06,
      elevation: 0,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
          side: BorderSide(color: Color.fromRGBO(0, 3, 42, 1))),
      color: Colors.transparent,
      onPressed: () => {changeForm()},
      child: Text(
        "Sign In",
        style: GoogleFonts.nunito(
          fontSize: 20,
          fontWeight: FontWeight.w800,
          textStyle: TextStyle(color: Color.fromRGBO(0, 3, 42, 1)),
        ),
      ),
    );
    return Container(
      decoration: BoxDecoration(color: Color.fromRGBO(195, 152, 202, 1)),
      child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(top: _height * 0.08),
            height: _height * 0.3,
            decoration: BoxDecoration(
                color: Colors.transparent,
                image: DecorationImage(
                  image: AssetImage('assets/images/logo.png'),
                )),
          ),
          Visibility(
            child: Login(),
            visible: _isVisible,
          ),
          Visibility(
            child: Register(),
            visible: !_isVisible,
          ),
          Container(
            margin: EdgeInsets.only(top: _height * 0.01),
            child: Align(
                child: Text(
              "Or",
              style: GoogleFonts.nunito(
                fontSize: 20,
                fontWeight: FontWeight.w700,
              ),
            )),
          ),
          Container(
            margin: EdgeInsets.only(top: _height * 0.01),
            child: Align(
              child: Visibility(
                child: newUser,
                visible: _isVisible,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: _height * 0.01),
            child: Align(
              child: Visibility(
                child: haveAccount,
                visible: !_isVisible,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
