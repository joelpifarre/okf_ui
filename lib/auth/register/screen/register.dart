import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final nameCtrl = TextEditingController();
  final emailCtrl = TextEditingController();
  final passwdCtrl = TextEditingController();
  final passwdCCtrl = TextEditingController();

  Widget build(BuildContext context) {
    double _width = MediaQuery.of(context).size.width;
    double _height = MediaQuery.of(context).size.height;

    return Stack(
      children: <Widget>[
        Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(
                left: _width * 0.05,
                top: _height * 0.02,
              ),
              margin: EdgeInsets.only(top: _height * 0.03),
              height: _height * 0.08,
              width: _width * 0.9,
              decoration: BoxDecoration(
                color: Color.fromRGBO(255, 255, 255, 0.6),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20),
                  topRight: Radius.circular(20),
                ),
              ),
              child: TextFormField(
                style: GoogleFonts.nunito(
                  fontWeight: FontWeight.w700,
                  fontSize: 18,
                ),
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(10.0, 14.0, 10.0, 14.0),
                    hintText: "Name",
                    border: InputBorder.none,
                    icon: Icon(
                      Icons.person_rounded,
                      size: 24,
                      color: Colors.grey,
                    )),
              ),
            ),
            Container(
              padding: EdgeInsets.only(
                left: _width * 0.05,
              ),
              height: _height * 0.08,
              width: _width * 0.9,
              decoration: BoxDecoration(
                color: Color.fromRGBO(255, 255, 255, 0.6),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(0),
                  topRight: Radius.circular(0),
                ),
              ),
              child: TextFormField(
                style: GoogleFonts.nunito(
                  fontWeight: FontWeight.w700,
                  fontSize: 18,
                ),
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(10.0, 14.0, 10.0, 14.0),
                    hintText: "Email",
                    border: InputBorder.none,
                    icon: Icon(
                      Icons.email_rounded,
                      size: 24,
                      color: Colors.grey,
                    )),
              ),
            ),
            Container(
              padding: EdgeInsets.only(
                left: _width * 0.05,
              ),
              height: _height * 0.08,
              width: _width * 0.9,
              decoration: BoxDecoration(
                color: Color.fromRGBO(255, 255, 255, 0.6),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(0),
                  topRight: Radius.circular(0),
                ),
              ),
              child: TextFormField(
                obscureText: true,
                style: GoogleFonts.nunito(
                  fontWeight: FontWeight.w700,
                  fontSize: 18,
                ),
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(10.0, 14.0, 10.0, 14.0),
                    hintText: "Password",
                    border: InputBorder.none,
                    icon: Icon(
                      Icons.lock_rounded,
                      size: 24,
                      color: Colors.grey,
                    )),
              ),
            ),
            Container(
              padding: EdgeInsets.only(
                left: _width * 0.05,
              ),
              height: _height * 0.08,
              width: _width * 0.9,
              decoration: BoxDecoration(
                color: Color.fromRGBO(255, 255, 255, 0.6),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(0),
                  topRight: Radius.circular(0),
                ),
              ),
              child: TextFormField(
                obscureText: true,
                style: GoogleFonts.nunito(
                  fontWeight: FontWeight.w700,
                  fontSize: 18,
                ),
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(10.0, 14.0, 10.0, 14.0),
                    hintText: "Confirm Password",
                    border: InputBorder.none,
                    icon: Icon(
                      Icons.lock_rounded,
                      size: 24,
                      color: Colors.grey,
                    )),
              ),
            ),
            Container(
              height: _height * 0.08,
              width: _width * 0.9,
              decoration: BoxDecoration(
                color: Color.fromRGBO(255, 255, 255, 0.6),
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(20),
                  bottomRight: Radius.circular(20),
                ),
              ),
              child: MaterialButton(
                elevation: 0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(20),
                        bottomRight: Radius.circular(20))),
                color: Color.fromRGBO(0, 3, 42, 1),
                onPressed: () => {},
                child: Text(
                  "Sign Up",
                  style: GoogleFonts.nunito(
                    fontSize: 20,
                    fontWeight: FontWeight.w800,
                    textStyle:
                        TextStyle(color: Color.fromRGBO(195, 159, 202, 1)),
                  ),
                ),
              ),
            ),
          ],
        )
      ],
    );
  }
}
