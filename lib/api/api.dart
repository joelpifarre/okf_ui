import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:okfantasy_app/api/sharedPref.dart';
import 'package:okfantasy_app/navigation/navigation.dart';
import 'package:shared_preferences/shared_preferences.dart';

const baseUrl = "http://192.168.1.209:8000/api";

class API {
  logIn(email, passwd, context) async {
    Map data = {"email": email, "password": passwd};
    var url = baseUrl + "/auth/login";
    var jsonResponse = {};
    String token = "";
    var response = await http.post(Uri.parse(url), body: data);
    if (response.statusCode == 200) {
      jsonResponse = json.decode(response.body);
      if (jsonResponse['success']) {
        token = jsonResponse['data']['token'].toString();
        SharedPref().save("token", token);
        getUserLeagues();
        getRoster();
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => Navigation()),
        );
      }
    }
  }

  getUserLeagues() async {
    var url = baseUrl + "/league/get";
    var jsonResponse = {};

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String authorization =
        prefs.getString("token").toString().replaceAll('"', '');
    var response = await http.post(Uri.parse(url), headers: <String, String>{
      'Content-Type': 'application/json', //add context-type
      'Accept': 'application/json',
      'Authorization': 'Bearer $authorization',
    });
    if (response.statusCode == 200) {
      jsonResponse = json.decode(response.body);
      if (jsonResponse['success']) {
        return jsonResponse['data'];
      }
    }
  }

  getRoster() async {
    var url = baseUrl + "/roster/get";
    var jsonResponse = {};
    Map data = {
      "league": "5",
    };

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String authorization =
        prefs.getString("token").toString().replaceAll('"', '');

    var response =
        await http.post(Uri.parse(url), body: data, headers: <String, String>{
      //add context-type
      'Accept': 'application/json',
      'Authorization': 'Bearer $authorization',
    });
    if (response.statusCode == 200) {
      jsonResponse = json.decode(response.body);
      if (jsonResponse['success']) {
        return jsonResponse['data'];
      }
    }
  }

  getPlayerByPos(String pos) async {
    var url = baseUrl + "/roster/pos";
    var jsonResponse = {};
    var playData = [];
    Map data = {"league": "5", "pos": pos};

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String authorization =
        prefs.getString("token").toString().replaceAll('"', '');

    var response =
        await http.post(Uri.parse(url), body: data, headers: <String, String>{
      //add context-type
      'Accept': 'application/json',
      'Authorization': 'Bearer $authorization',
    });
    if (response.statusCode == 200) {
      jsonResponse = json.decode(response.body);
      if (jsonResponse['success']) {
        if (jsonResponse['data'] != null) {
          playData = [jsonResponse['data']['name']];
        }
        // return jsonResponse['data'];
        return jsonResponse['data'];
      }
    }
  }

  getPlayerPosList(String pos) async {
    var url = baseUrl + "/roster/pos_filter";
    var jsonResponse = {};
    var playData = [];
    Map data = {"league": "5", "pos": pos};

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String authorization =
        prefs.getString("token").toString().replaceAll('"', '');

    var response =
        await http.post(Uri.parse(url), body: data, headers: <String, String>{
      //add context-type
      'Accept': 'application/json',
      'Authorization': 'Bearer $authorization',
    });
    if (response.statusCode == 200) {
      jsonResponse = json.decode(response.body);
      if (jsonResponse['success']) {
        // return jsonResponse['data'];
        debugPrint(jsonResponse['data'].toString());
        return jsonResponse['data'];
      }
    }
  }
}
