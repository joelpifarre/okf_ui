import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:okfantasy_app/api/api.dart';
import 'package:okfantasy_app/models/players.dart';
import 'package:okfantasy_app/roster/components/playerCard.dart';
import 'package:okfantasy_app/roster/components/playerList.dart';
import 'package:okfantasy_app/roster/screen/court.dart';

class Roster extends StatefulWidget {
  @override
  _RosterState createState() => _RosterState();
}

class _RosterState extends State<Roster> {
  bool _isVisible = true;
  var roster = new List<RPlayerClass>();

  @override
  void initState() {
    super.initState();
  }

  void changeView() {
    setState(() {
      _isVisible = !_isVisible;
    });
  }

  Widget build(BuildContext context) {
    var queryData = MediaQuery.of(context);

    return Container(
      color: Colors.white,
      child: Stack(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(
                    top: queryData.size.height * 0.01,
                    bottom: queryData.size.height * 0.01,
                    left: queryData.size.width * 0.05),
                child: Text(
                  "My Team",
                  style: GoogleFonts.nunito(
                    fontWeight: FontWeight.w700,
                    textStyle: TextStyle(
                      color: Color.fromRGBO(142, 118, 176, 1),
                      fontSize: 24,
                    ),
                  ),
                ),
              ),
              Row(
                children: <Widget>[
                  Container(
                    width: queryData.size.width * 0.24,
                    height: queryData.size.height * 0.042,
                    decoration: BoxDecoration(
                      color: _isVisible
                          ? Color.fromRGBO(195, 159, 202, 1)
                          : Colors.transparent,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        bottomLeft: Radius.circular(20),
                      ),
                      border: Border.all(
                        color: !_isVisible
                            ? Color.fromRGBO(195, 159, 202, 1)
                            : Colors.transparent,
                      ),
                    ),
                    child: Center(
                        child: MaterialButton(
                      child: Text(
                        "Roster",
                        style: GoogleFonts.nunito(
                          fontWeight: FontWeight.w700,
                          textStyle: TextStyle(
                            color: _isVisible
                                ? Color.fromRGBO(255, 255, 255, 1)
                                : Color.fromRGBO(195, 159, 202, 1),
                          ),
                        ),
                      ),
                      onPressed: () =>
                          {Court(), _isVisible ? null : changeView()},
                    )),
                  ),
                  Container(
                    margin: EdgeInsets.only(right: queryData.size.width * 0.04),
                    width: queryData.size.width * 0.24,
                    height: queryData.size.height * 0.042,
                    decoration: BoxDecoration(
                      color: !_isVisible
                          ? Color.fromRGBO(195, 159, 202, 1)
                          : Colors.transparent,
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(20),
                        bottomRight: Radius.circular(20),
                      ),
                      border: Border.all(
                        color: !_isVisible
                            ? Colors.transparent
                            : Color.fromRGBO(195, 159, 202, 1),
                      ),
                    ),
                    child: Center(
                        child: MaterialButton(
                      child: Text(
                        "Players",
                        style: GoogleFonts.nunito(
                          fontWeight: FontWeight.w700,
                          textStyle: TextStyle(
                            color: !_isVisible
                                ? Color.fromRGBO(255, 255, 255, 1)
                                : Color.fromRGBO(195, 159, 202, 1),
                          ),
                        ),
                      ),
                      onPressed: () => {
                        // _getRoster(),
                        !_isVisible ? null : changeView(),
                      },
                    )),
                  ),
                ],
              )
            ],
          ),
          Center(
            child: Visibility(
              child: Court(),
              visible: _isVisible,
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              top: queryData.size.height * 0.08,
            ),
            child: Visibility(
              child: RosterPLayerList(),
              visible: !_isVisible,
            ),
          ),
        ],
      ),
    );
  }
}
