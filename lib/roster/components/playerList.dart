import 'package:flutter/material.dart';
import 'package:okfantasy_app/api/api.dart';
import 'package:okfantasy_app/models/players.dart';
import 'package:okfantasy_app/roster/components/playerCard.dart';

class RosterPLayerList extends StatefulWidget {
  @override
  createState() => _RosterPLayerListState();
}

class _RosterPLayerListState extends State {
  var leagues = new List<RPlayerClass>();

  _getUserRoster() {
    API().getRoster().then((response) {
      setState(() {
        Iterable list = response;
        leagues = list.map((model) => RPlayerClass.fromJson(model)).toList();
      });
    });
  }

  initState() {
    super.initState();
    _getUserRoster();
  }

  dispose() {
    super.dispose();
  }

  @override
  build(context) {
    return ListView.builder(
      itemCount: leagues.length,
      itemBuilder: (context, index) {
        return player(leagues[index], false, context);
      },
    );
  }
}
