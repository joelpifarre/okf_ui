import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

showPlayerDetail(context) {
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return PlayerCardDetail();
      });
}

class PlayerCardDetail extends StatefulWidget {
  @override
  _PlayerCardDetailState createState() => _PlayerCardDetailState();
}

class _PlayerCardDetailState extends State<PlayerCardDetail> {
  bool _isVisible = true;

  @override
  void initState() {
    super.initState();
  }

  void changeView() {
    setState(() {
      _isVisible = !_isVisible;
    });
  }

  Widget build(BuildContext context) {
    var _height = MediaQuery.of(context).size.height;
    var _width = MediaQuery.of(context).size.width;
    Dialog playerDetail = Dialog(
      insetPadding: EdgeInsets.all(12),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      child: Container(
        height: _height * 0.7,
        width: _width * 1,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(20),
        ),
        child: Column(
          children: [
            Container(
              width: _width * 1,
              height: _height * 0.24,
              decoration: BoxDecoration(
                color: Colors.transparent,
              ),
              child: Row(
                children: <Widget>[playerInfoCard(context)],
              ),
            ),
            Container(
              width: _width * 1,
              height: _height * 0.46,
              color: Colors.transparent,
              child: Column(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(bottom: _height * 0.02),
                    height: _height * 0.06,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(left: _width * 0.01),
                          height: _height * 0.08,
                          width: _width * 0.14,
                          color: Colors.transparent,
                          child: goalsCard(context),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: _width * 0.01),
                          height: _height * 0.08,
                          width: _width * 0.14,
                          color: Colors.transparent,
                          child: assistsCard(context),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: _width * 0.01),
                          height: _height * 0.08,
                          width: _width * 0.14,
                          color: Colors.transparent,
                          child: blueCard(context),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: _width * 0.01),
                          height: _height * 0.08,
                          width: _width * 0.14,
                          color: Colors.transparent,
                          child: redCard(context),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: _width * 0.01),
                          height: _height * 0.08,
                          width: _width * 0.14,
                          color: Colors.transparent,
                          child: penaltyCard(context),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: _width * 0.01),
                          height: _height * 0.08,
                          width: _width * 0.14,
                          color: Colors.transparent,
                          child: freeShootCard(context),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: _width * 0.22),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          width: _width * 0.24,
                          height: _height * 0.042,
                          decoration: BoxDecoration(
                            color: _isVisible
                                ? Color.fromRGBO(195, 159, 202, 1)
                                : Colors.transparent,
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20),
                              bottomLeft: Radius.circular(20),
                            ),
                            border: Border.all(
                              color: !_isVisible
                                  ? Color.fromRGBO(195, 159, 202, 1)
                                  : Colors.transparent,
                            ),
                          ),
                          child: Center(
                            child: MaterialButton(
                              child: Text(
                                "Stats",
                                style: GoogleFonts.nunito(
                                  fontWeight: FontWeight.w700,
                                  textStyle: TextStyle(
                                    color: _isVisible
                                        ? Color.fromRGBO(255, 255, 255, 1)
                                        : Color.fromRGBO(195, 159, 202, 1),
                                  ),
                                ),
                              ),
                              onPressed: () =>
                                  {_isVisible ? null : changeView()},
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(right: _width * 0.04),
                          width: _width * 0.24,
                          height: _height * 0.042,
                          decoration: BoxDecoration(
                            color: !_isVisible
                                ? Color.fromRGBO(195, 159, 202, 1)
                                : Colors.transparent,
                            borderRadius: BorderRadius.only(
                              topRight: Radius.circular(20),
                              bottomRight: Radius.circular(20),
                            ),
                            border: Border.all(
                              color: !_isVisible
                                  ? Colors.transparent
                                  : Color.fromRGBO(195, 159, 202, 1),
                            ),
                          ),
                          child: Center(
                              child: MaterialButton(
                            child: Text(
                              "Matches",
                              style: GoogleFonts.nunito(
                                fontWeight: FontWeight.w700,
                                textStyle: TextStyle(
                                  color: !_isVisible
                                      ? Color.fromRGBO(255, 255, 255, 1)
                                      : Color.fromRGBO(195, 159, 202, 1),
                                ),
                              ),
                            ),
                            onPressed: () =>
                                {!_isVisible ? null : changeView()},
                          )),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    child: Visibility(
                      visible: _isVisible,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                overallPtsCard(context),
                                lastMonthCard(context),
                              ],
                            ),
                          ),
                          Container(
                            child: Column(
                              children: <Widget>[avgPtsCard(context)],
                            ),
                          ),
                          Container(
                            child: Column(
                              children: <Widget>[
                                lastWeekCard(context),
                                rankCard(context),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    color: Colors.transparent,
                    child: Visibility(
                      visible: !_isVisible,
                      child: Column(
                        children: [
                          // matchListTitle(context),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
    return playerDetail;
  }

  Widget rankCard(context) {
    var _height = MediaQuery.of(context).size.height;
    var _width = MediaQuery.of(context).size.width;
    return Container(
      margin: EdgeInsets.only(top: _height * 0.03),
      height: _height * 0.12,
      width: _width * 0.24,
      decoration: BoxDecoration(
        color: Colors.transparent,
      ),
      child: Center(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Rank",
                style: GoogleFonts.nunito(
                  fontSize: 12,
                  fontWeight: FontWeight.w700,
                  color: Colors.grey[700],
                ),
              ),
              Text(
                "#0",
                style: GoogleFonts.nunito(
                    fontSize: 26, fontWeight: FontWeight.w700),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget lastWeekCard(context) {
    var _height = MediaQuery.of(context).size.height;
    var _width = MediaQuery.of(context).size.width;
    return Container(
      margin: EdgeInsets.only(top: _height * 0.03),
      height: _height * 0.12,
      width: _width * 0.24,
      decoration: BoxDecoration(
        color: Colors.transparent,
      ),
      child: Center(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Last Week",
                style: GoogleFonts.nunito(
                  fontSize: 12,
                  fontWeight: FontWeight.w700,
                  color: Colors.grey[700],
                ),
              ),
              Text(
                "0",
                style: GoogleFonts.nunito(
                    fontSize: 26, fontWeight: FontWeight.w700),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget lastMonthCard(context) {
    var _height = MediaQuery.of(context).size.height;
    var _width = MediaQuery.of(context).size.width;
    return Container(
      margin: EdgeInsets.only(top: _height * 0.03),
      height: _height * 0.12,
      width: _width * 0.24,
      decoration: BoxDecoration(
        color: Colors.transparent,
      ),
      child: Center(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Last Months",
                style: GoogleFonts.nunito(
                  fontSize: 12,
                  fontWeight: FontWeight.w700,
                  color: Colors.grey[700],
                ),
              ),
              Text(
                "0",
                style: GoogleFonts.nunito(
                    fontSize: 26, fontWeight: FontWeight.w700),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget overallPtsCard(context) {
    var _height = MediaQuery.of(context).size.height;
    var _width = MediaQuery.of(context).size.width;
    return Container(
      margin: EdgeInsets.only(top: _height * 0.03),
      height: _height * 0.12,
      width: _width * 0.24,
      decoration: BoxDecoration(
        color: Colors.transparent,
      ),
      child: Center(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Overall PTS",
                style: GoogleFonts.nunito(
                  fontSize: 12,
                  fontWeight: FontWeight.w700,
                  color: Colors.grey[700],
                ),
              ),
              Text(
                "0",
                style: GoogleFonts.nunito(
                    fontSize: 26, fontWeight: FontWeight.w700),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget avgPtsCard(context) {
    var _height = MediaQuery.of(context).size.height;
    var _width = MediaQuery.of(context).size.width;
    return Container(
      height: _height * 0.2,
      width: _width * 0.4,
      decoration: BoxDecoration(
        color: Color.fromRGBO(0, 0, 0, 0.1),
        shape: BoxShape.circle,
      ),
      child: Center(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "AVG",
                style: GoogleFonts.nunito(
                    fontSize: 22, fontWeight: FontWeight.w700),
              ),
              Text(
                "0",
                style: GoogleFonts.nunito(
                    fontSize: 50, fontWeight: FontWeight.w700),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget playerInfoCard(context) {
    var _height = MediaQuery.of(context).size.height;
    var _width = MediaQuery.of(context).size.width;
    return Container(
      margin: EdgeInsets.only(left: _width * 0.02),
      height: _height * 0.22,
      width: _width * 0.9,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Color(0xFFc39fca),
      ),
      child: Row(
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                margin:
                    EdgeInsets.only(top: _height * 0.02, left: _width * 0.03),
                child: Text(
                  "Player Name",
                  style: GoogleFonts.nunito(
                      fontSize: 22,
                      fontWeight: FontWeight.w700,
                      color: Colors.black),
                ),
              ),
              Container(
                height: _height * 0.05,
                width: _width * 0.5,
                decoration: BoxDecoration(
                  color: Colors.transparent,
                ),
                child: Row(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(
                          top: _height * 0.02, left: _width * 0.024),
                      height: _height * 0.04,
                      width: _width * 0.08,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage('assets/images/logo.png'),
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                          top: _height * 0.02, left: _width * 0.024),
                      child: Text(
                        "Team Name",
                        style: GoogleFonts.nunito(
                            fontSize: 20,
                            fontWeight: FontWeight.w500,
                            color: Colors.black),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: _height * 0.044),
                child: Row(
                  children: <Widget>[
                    Container(
                      height: _height * 0.035,
                      width: _width * 0.07,
                      decoration: BoxDecoration(
                        color: Colors.orangeAccent,
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: Center(
                        child: Text(
                          "GK",
                          style: GoogleFonts.nunito(
                            color: Colors.white,
                            fontSize: 12,
                            fontWeight: FontWeight.w800,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                          left: _width * 0.04, right: _width * 0.04),
                      height: _height * 0.035,
                      width: _width * 0.24,
                      decoration: BoxDecoration(
                        color: Color.fromRGBO(0, 0, 0, 0.3),
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Row(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: _width * 0.01),
                            child: Icon(
                              Icons.euro_rounded,
                              size: 18,
                              color: Colors.white,
                            ),
                          ),
                          Center(
                            child: Container(
                              margin: EdgeInsets.only(left: _width * 0.026),
                              child: Text(
                                "200 K",
                                style: GoogleFonts.nunito(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w700,
                                    color: Colors.white),
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
          Container(
            margin: EdgeInsets.only(left: _width * 0.02),
            height: _height * 0.5,
            width: _width * 0.34,
            decoration: BoxDecoration(
              color: Colors.transparent,
              image: DecorationImage(
                image: AssetImage('assets/images/player.png'),
                fit: BoxFit.contain,
              ),
            ),
          ),
        ],
      ),
    );
  }

//stadistic
//
//
  Widget goalsCard(context) {
    var _height = MediaQuery.of(context).size.height;
    var _width = MediaQuery.of(context).size.width;
    return Container(
      height: _height * 0.12,
      width: _width * 0.24,
      decoration: BoxDecoration(
        color: Colors.transparent,
      ),
      child: Center(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Goals",
                style: GoogleFonts.nunito(
                  fontSize: 10,
                  fontWeight: FontWeight.w700,
                  color: Colors.grey[700],
                ),
              ),
              Text(
                "0",
                style: GoogleFonts.nunito(
                    fontSize: 20, fontWeight: FontWeight.w700),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget assistsCard(context) {
    var _height = MediaQuery.of(context).size.height;
    var _width = MediaQuery.of(context).size.width;
    return Container(
      height: _height * 0.12,
      width: _width * 0.24,
      decoration: BoxDecoration(
        color: Colors.transparent,
      ),
      child: Center(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Assists",
                style: GoogleFonts.nunito(
                  fontSize: 10,
                  fontWeight: FontWeight.w700,
                  color: Colors.grey[700],
                ),
              ),
              Text(
                "0",
                style: GoogleFonts.nunito(
                    fontSize: 20, fontWeight: FontWeight.w700),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget blueCard(context) {
    var _height = MediaQuery.of(context).size.height;
    var _width = MediaQuery.of(context).size.width;
    return Container(
      height: _height * 0.12,
      width: _width * 0.24,
      decoration: BoxDecoration(
        color: Colors.transparent,
      ),
      child: Center(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Blue",
                style: GoogleFonts.nunito(
                  fontSize: 10,
                  fontWeight: FontWeight.w700,
                  color: Colors.grey[700],
                ),
              ),
              Text(
                "0",
                style: GoogleFonts.nunito(
                    fontSize: 20, fontWeight: FontWeight.w700),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget redCard(context) {
    var _height = MediaQuery.of(context).size.height;
    var _width = MediaQuery.of(context).size.width;
    return Container(
      height: _height * 0.12,
      width: _width * 0.24,
      decoration: BoxDecoration(
        color: Colors.transparent,
      ),
      child: Center(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Red",
                style: GoogleFonts.nunito(
                  fontSize: 10,
                  fontWeight: FontWeight.w700,
                  color: Colors.grey[700],
                ),
              ),
              Text(
                "0",
                style: GoogleFonts.nunito(
                    fontSize: 20, fontWeight: FontWeight.w700),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget penaltyCard(context) {
    var _height = MediaQuery.of(context).size.height;
    var _width = MediaQuery.of(context).size.width;
    return Container(
      height: _height * 0.12,
      width: _width * 0.24,
      decoration: BoxDecoration(
        color: Colors.transparent,
      ),
      child: Center(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "PE",
                style: GoogleFonts.nunito(
                  fontSize: 10,
                  fontWeight: FontWeight.w700,
                  color: Colors.grey[700],
                ),
              ),
              Text(
                "0/0",
                style: GoogleFonts.nunito(
                    fontSize: 20, fontWeight: FontWeight.w700),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget freeShootCard(context) {
    var _height = MediaQuery.of(context).size.height;
    var _width = MediaQuery.of(context).size.width;
    return Container(
      height: _height * 0.12,
      width: _width * 0.24,
      decoration: BoxDecoration(
        color: Colors.transparent,
      ),
      child: Center(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "FS",
                style: GoogleFonts.nunito(
                  fontSize: 10,
                  fontWeight: FontWeight.w700,
                  color: Colors.grey[700],
                ),
              ),
              Text(
                "0/0",
                style: GoogleFonts.nunito(
                    fontSize: 20, fontWeight: FontWeight.w700),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget matchListTitle(context) {
    var _height = MediaQuery.of(context).size.height;
    var _width = MediaQuery.of(context).size.width;
    return Row(
      children: <Widget>[
        Container(
          height: _height * 0.03,
          width: _width * 0.15,
          child: Center(
            child: Text(
              "Week",
              style: GoogleFonts.nunito(
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
        ),
        Container(
          height: _height * 0.03,
          width: _width * 0.15,
          child: Center(
            child: Text(
              "Rival",
              style: GoogleFonts.nunito(
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
        ),
        Container(
          height: _height * 0.03,
          width: _width * 0.15,
          child: Center(
            child: Text(
              "G",
              style: GoogleFonts.nunito(
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
        ),
        Container(
          height: _height * 0.03,
          width: _width * 0.15,
          child: Center(
            child: Text(
              "As",
              style: GoogleFonts.nunito(
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
        ),
        Container(
          height: _height * 0.03,
          width: _width * 0.15,
          child: Center(
            child: Text(
              "FS",
              style: GoogleFonts.nunito(
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
        ),
        Container(
          height: _height * 0.03,
          width: _width * 0.15,
          child: Center(
            child: Text(
              "PE",
              style: GoogleFonts.nunito(
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget matchListItemTitle(context, w, t, g, a, b, r, fd, pn) {
    var _height = MediaQuery.of(context).size.height;
    var _width = MediaQuery.of(context).size.width;
    return Row(
      children: [
        Container(
          height: _height * 0.05,
          width: _width * 0.115,
          color: Colors.transparent,
          child: Center(
            child: Text(w),
          ),
        ),
        Container(
          height: _height * 0.05,
          width: _width * 0.115,
          color: Colors.transparent,
          child: Center(
            child: Text(t),
          ),
        ),
        Container(
          height: _height * 0.05,
          width: _width * 0.115,
          color: Colors.transparent,
          child: Center(
            child: Text(g),
          ),
        ),
        Container(
          height: _height * 0.05,
          width: _width * 0.115,
          color: Colors.transparent,
          child: Center(
            child: Text(a),
          ),
        ),
        Container(
          height: _height * 0.05,
          width: _width * 0.115,
          color: Colors.transparent,
          child: Center(
            child: Text(b),
          ),
        ),
        Container(
          height: _height * 0.05,
          width: _width * 0.115,
          color: Colors.transparent,
          child: Center(
            child: Text(r),
          ),
        ),
        Container(
          height: _height * 0.05,
          width: _width * 0.115,
          color: Colors.transparent,
          child: Center(
            child: Text(fd),
          ),
        ),
        Container(
          height: _height * 0.05,
          width: _width * 0.115,
          color: Colors.transparent,
          child: Center(
            child: Text(pn),
          ),
        ),
      ],
    );
  }
}
