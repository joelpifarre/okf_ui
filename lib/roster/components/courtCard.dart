import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:okfantasy_app/api/api.dart';
import 'package:okfantasy_app/roster/components/playerCard.dart';

class CourtCard extends StatefulWidget {
  @override
  _CourtCardState createState() => _CourtCardState();
}

class _CourtCardState extends State<CourtCard> {
  var playerFW1, playerFW2, playerDF1, playerDF2, playerGK;
  var playerC, playerSFW, playerSDF, playerSGK;

  var posL = ['FW1', 'FW2', 'DF1', 'DF2', 'GK', 'C', 'SFW', 'SDF', 'SGK'];

  _getPlayerPos() {
    for (var i = 0; i < posL.length; i++) {
      API().getPlayerByPos(posL[i]).then((res) {
        switch (posL[i]) {
          case 'FW1':
            this.playerFW1 = res;
            debugPrint(this.playerFW1['image'].toString());
            break;
          case 'FW2':
            this.playerFW2 = res;
            debugPrint(this.playerFW2.toString());
            break;
          case 'DF1':
            this.playerDF1 = res;
            debugPrint(this.playerDF1.toString());
            break;
          case 'DF2':
            this.playerDF2 = res;
            debugPrint(this.playerDF2.toString());
            break;
          case 'GK':
            this.playerGK = res;
            debugPrint(this.playerGK.toString());
            break;
          case 'SFW':
            this.playerSFW = res;
            debugPrint(this.playerSFW.toString());
            break;
          case 'SDF':
            this.playerSDF = res;
            debugPrint(this.playerSDF.toString());
            break;
          case 'SGK':
            this.playerSGK = res;
            debugPrint(this.playerSGK.toString());
            break;
          case 'C':
            this.playerC = res;
            debugPrint(this.playerC.toString());
            break;
          default:
        }
      });
    }
  }

  initState() {
    super.initState();
    _getPlayerPos();
  }

  dispose() {
    super.dispose();
  }

  @override
  build(context) {
    var queryData = MediaQuery.of(context);
    return MaterialButton(
      minWidth: queryData.size.width * 0.12,
      child: Container(
          width: queryData.size.width * 0.12,
          height: queryData.size.height * 0.08,
          decoration: BoxDecoration(
              color: Color.fromRGBO(0, 0, 0, 0.5),
              borderRadius: BorderRadius.circular(10),
              image: DecorationImage(
                image: NetworkImage(
                    "https://ns3104249.ip-54-37-85.eu/rfep/images//fichas/11762.jpg"),
              )),
          child: Column(
            children: [],
          )),
      onPressed: () => {},
    );
  }
}
