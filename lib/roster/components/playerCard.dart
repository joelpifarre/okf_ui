import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:okfantasy_app/roster/components/courtInitialPlayers/intitialDF1.dart';
import 'package:okfantasy_app/roster/components/courtInitialPlayers/intitialDF2.dart';
import 'package:okfantasy_app/roster/components/courtInitialPlayers/intitialFW1.dart';
import 'package:okfantasy_app/roster/components/courtInitialPlayers/intitialFW2.dart';
import 'package:okfantasy_app/roster/components/courtInitialPlayers/intitialGK.dart';
import 'package:okfantasy_app/roster/components/courtInitialPlayers/intitialSDF.dart';
import 'package:okfantasy_app/roster/components/courtInitialPlayers/intitialSFW.dart';
import 'package:okfantasy_app/roster/components/courtInitialPlayers/intitialSGK.dart';
import 'package:okfantasy_app/roster/components/player.dart';

Widget playerCard(pos, poss, context) {
  var player;

  switch (pos) {
    case "FW1":
      player = new InitFW1();
      break;
    case "FW2":
      player = new InitFW2();
      break;
    case "DF1":
      player = new InitDF1();
      break;
    case "DF2":
      player = new InitDF2();
      break;
    case "GK":
      player = new InitGK();
      break;
    case "C":
      player = new InitGK();
      break;
    case "SFW":
      player = new InitSFW();
      break;
    case "SDF":
      player = new InitSDF();
      break;
    case "SGK":
      player = new InitSGK();
      break;

    default:
  }

  return Material(
      color: Colors.transparent,
      borderRadius: BorderRadius.circular(10),
      child: player);
}

Widget player(data, transfer, context) {
  var queryData = MediaQuery.of(context);

  return Container(
    margin: EdgeInsets.only(top: queryData.size.height * 0.01),
    height: queryData.size.height * 0.1,
    width: queryData.size.width * 1,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Container(
          height: queryData.size.height * 1,
          width: queryData.size.width * 0.014,
          decoration: BoxDecoration(
            color: data.initPos == 'FW1' ||
                    data.initPos == 'FW2' ||
                    data.initPos == 'DF1' ||
                    data.initPos == 'DF2' ||
                    data.initPos == 'DF3' ||
                    data.initPos == 'GK'
                ? Color.fromRGBO(195, 159, 202, 1)
                : data.initPos == 'SFW' ||
                        data.initPos == 'SDF' ||
                        data.initPos == 'SGK'
                    ? Color.fromRGBO(195, 159, 202, 0.5)
                    : Colors.transparent,
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(20),
              bottomRight: Radius.circular(20),
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(
            left: queryData.size.width * 0.01,
            right: queryData.size.width * 0.01,
          ),
          height: queryData.size.height * 1,
          width: queryData.size.width * 0.1,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                height: queryData.size.height * 0.03,
                width: queryData.size.width * 0.06,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(6),
                  color: Colors.transparent,
                  image: DecorationImage(
                    image: AssetImage('assets/images/logo.png'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Container(
                height: queryData.size.height * 0.03,
                width: queryData.size.width * 0.06,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(6),
                  color: Colors.black,
                ),
                child: Center(
                  child: Text(
                    "0",
                    style: GoogleFonts.nunito(
                      fontSize: 10,
                      textStyle: TextStyle(color: Colors.white),
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
              ),

              //POS SQUARE

              Container(
                height: queryData.size.height * 0.03,
                width: queryData.size.width * 0.06,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(6),
                    color: data.pos == 'FW'
                        ? Colors.redAccent
                        : data.pos == 'DF'
                            ? Colors.blueAccent
                            : data.pos == 'GK'
                                ? Colors.orangeAccent
                                : null),
                child: Center(
                  child: Text(
                    data.pos.toString().toUpperCase(),
                    style: GoogleFonts.nunito(
                      fontSize: 10,
                      fontWeight: FontWeight.w700,
                      textStyle: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              ),

              //
            ],
          ),
        ),
        Container(
          height: queryData.size.height * 0.1,
          width: queryData.size.width * 0.16,
          decoration: BoxDecoration(
            color: Color.fromRGBO(0, 0, 0, 0.2),
            image: DecorationImage(
              image: data.image == null
                  ? AssetImage('assets/images/player.png')
                  : NetworkImage(
                      "https://ns3104249.ip-54-37-85.eu/rfep/images//fichas/11762.jpg"),
              fit: BoxFit.cover,
            ),
            borderRadius: BorderRadius.circular(10),
          ),
          child: MaterialButton(
            onPressed: () => {showPlayerDetail(context)},
          ),
        ),
        Container(
          margin: EdgeInsets.only(
            left: queryData.size.width * 0.01,
          ),
          height: queryData.size.height * 0.1,
          width: queryData.size.width * 0.44,
          decoration: BoxDecoration(
            color: Colors.transparent,
            borderRadius: BorderRadius.circular(10),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(
                  left: queryData.size.width * 0.05,
                ),
                child: Text(
                  data.name,
                  style: GoogleFonts.nunito(
                    fontSize: 16,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                  left: queryData.size.width * 0.05,
                ),
                child: Row(
                  children: <Widget>[
                    Text(
                      "€ 100.000",
                      style: GoogleFonts.nunito(
                          fontSize: 12,
                          fontWeight: FontWeight.w700,
                          textStyle: TextStyle(color: Colors.grey[500])),
                    ),
                    Icon(
                      Icons.expand_more_rounded,
                      color: Colors.transparent,
                    )
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                  left: queryData.size.width * 0.05,
                ),
                child: Text(
                  "AVG 0,0",
                  style: GoogleFonts.nunito(
                    fontSize: 12,
                    fontWeight: FontWeight.w700,
                    textStyle: TextStyle(
                      color: Colors.grey[500],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        Container(
          height: queryData.size.height * 0.1,
          width: queryData.size.width * 0.22,
          decoration: BoxDecoration(
            color: Colors.transparent,
            borderRadius: BorderRadius.circular(10),
          ),
          child: Center(
            child: Container(
              margin: EdgeInsets.only(
                right: queryData.size.width * 0.01,
              ),
              width: queryData.size.width * 0.4,
              height: queryData.size.height * 0.04,
              decoration: BoxDecoration(
                color: !transfer
                    ? Color.fromRGBO(195, 159, 202, 1)
                    : Colors.transparent,
                border: Border.all(
                  color: transfer
                      ? Color.fromRGBO(195, 159, 202, 1)
                      : Colors.transparent,
                ),
                borderRadius: BorderRadius.circular(8),
              ),
              child: Center(
                child: Container(
                  child: Row(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(
                          left: queryData.size.width * 0.018,
                          right: queryData.size.width * 0.01,
                        ),
                        child: Icon(
                          !transfer
                              ? Icons.payments_rounded
                              : Icons.euro_rounded,
                          size: 16,
                          color: !transfer
                              ? Colors.white
                              : Color.fromRGBO(195, 159, 202, 1),
                        ),
                      ),
                      Text(
                        !transfer ? "Transfer" : "150.000",
                        style: GoogleFonts.nunito(
                            fontWeight: FontWeight.w700,
                            color: !transfer
                                ? Colors.white
                                : Color.fromRGBO(195, 159, 202, 1),
                            fontSize: 12),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    ),
  );

  //
}
