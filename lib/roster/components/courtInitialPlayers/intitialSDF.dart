import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:okfantasy_app/api/api.dart';
import 'package:okfantasy_app/roster/components/playerCard.dart';

class InitSDF extends StatefulWidget {
  @override
  _InitSDFState createState() => _InitSDFState();
}

class _InitSDFState extends State<InitSDF> {
  var player;
  var img;
  _getPlayerPos() {
    API().getPlayerByPos("SDF").then((res) {
      setState(() {
        this.player = res;
      });
    });
  }

  initState() {
    super.initState();
    _getPlayerPos();
  }

  dispose() {
    super.dispose();
  }

  @override
  build(context) {
    var queryData = MediaQuery.of(context);
    return MaterialButton(
      minWidth: queryData.size.width * 0.12,
      child: Container(
          width: queryData.size.width * 0.12,
          height: queryData.size.height * 0.08,
          decoration: BoxDecoration(
              color: Color.fromRGBO(0, 0, 0, 0.5),
              borderRadius: BorderRadius.circular(10),
              image: DecorationImage(
                  image: this.player != null
                      ? this.player['image'] != null
                          ? this.img = AssetImage(
                              "https://ns3104249.ip-54-37-85.eu/rfep/images//fichas/11762.jpg")
                          : this.img = AssetImage("assets/images/player.png")
                      : this.img = AssetImage("assets/images/add.png"))),
          child: Column(
            children: [],
          )),
      onPressed: () => {},
    );
  }
}
