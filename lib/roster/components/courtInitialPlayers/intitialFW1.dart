import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:okfantasy_app/api/api.dart';
import 'package:okfantasy_app/models/players.dart';
import 'package:okfantasy_app/roster/components/playerCard.dart';

class InitFW1 extends StatefulWidget {
  @override
  _InitFW1State createState() => _InitFW1State();
}

class _InitFW1State extends State<InitFW1> {
  var player;

  _getPlayerPos() {
    API().getPlayerByPos("FW1").then((res) {
      setState(() {
        this.player = res;
      });
    });
  }

  initState() {
    super.initState();
    _getPlayerPos();
  }

  dispose() {
    super.dispose();
  }

  @override
  build(context) {
    var queryData = MediaQuery.of(context);
    return MaterialButton(
      minWidth: queryData.size.width * 0.12,
      child: Container(
          width: queryData.size.width * 0.12,
          height: queryData.size.height * 0.08,
          decoration: BoxDecoration(
              color: Color.fromRGBO(0, 0, 0, 0.5),
              borderRadius: BorderRadius.circular(10),
              image: DecorationImage(
                image: this.player != null
                    ? this.player['image'] != null
                        ? NetworkImage(
                            "https://ns3104249.ip-54-37-85.eu/rfep/images//fichas/11762.jpg")
                        : AssetImage("assets/images/player.png")
                    : AssetImage("assets/images/add.png"),
              )),
          child: Column(
            children: [],
          )),
      onPressed: () => {_settingModalBottomSheet(context)},
    );
  }

  void _settingModalBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(child: FWList());
        });
  }
}

class FWList extends StatefulWidget {
  @override
  createState() => _FWListState();
}

class _FWListState extends State {
  var leagues = new List<RPlayerClass>();

  _getUserRoster() {
    API().getPlayerPosList("FW").then((response) {
      setState(() {
        Iterable list = response;
        leagues = list.map((model) => RPlayerClass.fromJson(model)).toList();
      });
    });
  }

  initState() {
    super.initState();
    _getUserRoster();
  }

  dispose() {
    super.dispose();
  }

  @override
  build(context) {
    return ListView.builder(
      itemCount: leagues.length,
      itemBuilder: (context, index) {
        return player(leagues[index], false, context);
      },
    );
  }
}

class DFList extends StatefulWidget {
  @override
  createState() => _DFListState();
}

class _DFListState extends State {
  var leagues = new List<RPlayerClass>();

  _getUserRoster() {
    API().getPlayerPosList("DF").then((response) {
      setState(() {
        Iterable list = response;
        leagues = list.map((model) => RPlayerClass.fromJson(model)).toList();
      });
    });
  }

  initState() {
    super.initState();
    _getUserRoster();
  }

  dispose() {
    super.dispose();
  }

  @override
  build(context) {
    return ListView.builder(
      itemCount: leagues.length,
      itemBuilder: (context, index) {
        return player(leagues[index], false, context);
      },
    );
  }
}

class GKList extends StatefulWidget {
  @override
  createState() => _GKListState();
}

class _GKListState extends State {
  var leagues = new List<RPlayerClass>();

  _getUserRoster() {
    API().getPlayerPosList("GK").then((response) {
      setState(() {
        Iterable list = response;
        leagues = list.map((model) => RPlayerClass.fromJson(model)).toList();
      });
    });
  }

  initState() {
    super.initState();
    _getUserRoster();
  }

  dispose() {
    super.dispose();
  }

  @override
  build(context) {
    return ListView.builder(
      itemCount: leagues.length,
      itemBuilder: (context, index) {
        return player(leagues[index], false, context);
      },
    );
  }
}
