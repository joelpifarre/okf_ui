import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:okfantasy_app/api/api.dart';
import 'package:okfantasy_app/roster/components/courtInitialPlayers/intitialFW1.dart';
import 'package:okfantasy_app/roster/components/playerCard.dart';

class InitGK extends StatefulWidget {
  @override
  _InitGKState createState() => _InitGKState();
}

class _InitGKState extends State<InitGK> {
  var player;
  AssetImage noImage = AssetImage("assets/image/player.png");

  _getPlayerPos() {
    API().getPlayerByPos("GK").then((res) {
      setState(() {
        this.player = res;
      });
    });
  }

  initState() {
    super.initState();
    _getPlayerPos();
  }

  dispose() {
    super.dispose();
  }

  @override
  build(context) {
    var queryData = MediaQuery.of(context);
    return MaterialButton(
      minWidth: queryData.size.width * 0.12,
      child: Container(
          width: queryData.size.width * 0.12,
          height: queryData.size.height * 0.08,
          decoration: BoxDecoration(
              color: Color.fromRGBO(0, 0, 0, 0.5),
              borderRadius: BorderRadius.circular(10),
              image: DecorationImage(
                  image: this.player == null
                      ? AssetImage("assets/images/add.png")
                      : this.player['image'] != null
                          ? NetworkImage(
                              "https://ns3104249.ip-54-37-85.eu/rfep/images//fichas/11762.jpg")
                          : noImage)),
          child: Column(
            children: [],
          )),
      onPressed: () => {_settingModalBottomSheet(context)},
    );
  }

  void _settingModalBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(child: GKList());
        });
  }
}
