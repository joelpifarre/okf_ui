import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:okfantasy_app/api/api.dart';
import 'package:okfantasy_app/roster/components/playerCard.dart';

class Court extends StatefulWidget {
  @override
  _CourtState createState() => _CourtState();
}

class _CourtState extends State<Court> {
  build(BuildContext context) {
    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;
    return Stack(
      children: <Widget>[
        Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: _height * 0.1),
              height: _height * 0.46,
              width: _width * 0.9,
              decoration: BoxDecoration(
                color: Colors.amber,
                borderRadius: BorderRadius.circular(20),
                image: DecorationImage(
                    image: AssetImage('assets/images/court.png'),
                    fit: BoxFit.cover),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Container(
                    height: _height * 0.1,
                    color: Colors.transparent,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        playerCard("FW1", "FW", context),
                        playerCard("FW2", "FW", context),
                      ],
                    ),
                  ),
                  Container(
                    height: _height * 0.1,
                    color: Colors.transparent,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        playerCard("DF1", "DF", context),
                        playerCard("DF2", "DF", context),
                      ],
                    ),
                  ),
                  Container(
                    height: _height * 0.1,
                    color: Colors.transparent,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        playerCard("GK", "GK", context),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: _height * 0.1,
              width: _width * 0.9,
              decoration: BoxDecoration(
                color: Color.fromRGBO(195, 159, 202, 1),
                borderRadius: BorderRadius.circular(20),
              ),
              margin: EdgeInsets.only(top: _height * 0.04),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  playerCard("C", "C", context),
                  playerCard("SFW", "FW", context),
                  playerCard("SDF", "DF", context),
                  playerCard("SGK", "GK", context),
                ],
              ),
            ),
          ],
        )
      ],
    );
  }
}
