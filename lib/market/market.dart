import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Market extends StatefulWidget {
  @override
  _MarketState createState() => _MarketState();
}

class _MarketState extends State<Market> {
  bool _isVisible = true;

  @override
  void initState() {
    super.initState();
  }

  void changeView() {
    setState(() {
      _isVisible = !_isVisible;
    });
  }

  Widget build(BuildContext context) {
    var _height = MediaQuery.of(context).size.height;
    var _width = MediaQuery.of(context).size.width;

    return Container(
      color: Colors.white,
      child: Stack(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(
                    top: _height * 0.01,
                    bottom: _height * 0.01,
                    left: _width * 0.05),
                child: Text(
                  "Market",
                  style: GoogleFonts.nunito(
                    fontWeight: FontWeight.w700,
                    textStyle: TextStyle(
                      color: Color.fromRGBO(142, 118, 176, 1),
                      fontSize: 24,
                    ),
                  ),
                ),
              ),
              Row(
                children: <Widget>[
                  Container(
                    width: _width * 0.24,
                    height: _height * 0.042,
                    decoration: BoxDecoration(
                      color: _isVisible
                          ? Color.fromRGBO(195, 159, 202, 1)
                          : Colors.transparent,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        bottomLeft: Radius.circular(20),
                      ),
                      border: Border.all(
                        color: !_isVisible
                            ? Color.fromRGBO(195, 159, 202, 1)
                            : Colors.transparent,
                      ),
                    ),
                    child: Center(
                        child: MaterialButton(
                      child: Text(
                        "Transfer",
                        style: GoogleFonts.nunito(
                          fontWeight: FontWeight.w700,
                          textStyle: TextStyle(
                            color: _isVisible
                                ? Color.fromRGBO(255, 255, 255, 1)
                                : Color.fromRGBO(195, 159, 202, 1),
                          ),
                        ),
                      ),
                      onPressed: () => {_isVisible ? null : changeView()},
                    )),
                  ),
                  Container(
                    margin: EdgeInsets.only(right: _width * 0.04),
                    width: _width * 0.24,
                    height: _height * 0.042,
                    decoration: BoxDecoration(
                      color: !_isVisible
                          ? Color.fromRGBO(195, 159, 202, 1)
                          : Colors.transparent,
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(20),
                        bottomRight: Radius.circular(20),
                      ),
                      border: Border.all(
                        color: !_isVisible
                            ? Colors.transparent
                            : Color.fromRGBO(195, 159, 202, 1),
                      ),
                    ),
                    child: Center(
                        child: MaterialButton(
                      child: Text(
                        "Offers",
                        style: GoogleFonts.nunito(
                          fontWeight: FontWeight.w700,
                          textStyle: TextStyle(
                            color: !_isVisible
                                ? Color.fromRGBO(255, 255, 255, 1)
                                : Color.fromRGBO(195, 159, 202, 1),
                          ),
                        ),
                      ),
                      onPressed: () => {!_isVisible ? null : changeView()},
                    )),
                  ),
                ],
              )
            ],
          ),
          Center(
            child: Visibility(
              child: Container(),
              visible: _isVisible,
            ),
          ),
          Container(
              margin: EdgeInsets.only(
                top: _height * 0.08,
              ),
              child: Visibility(
                child: ListView(
                  children: <Widget>[],
                ),
                visible: !_isVisible,
              ))
        ],
      ),
    );
  }
}
