import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class League extends StatefulWidget {
  @override
  _LeagueState createState() => _LeagueState();
}

class _LeagueState extends State<League> {
  bool _isVisible = true;

  @override
  void initState() {
    super.initState();
  }

  void changeView() {
    setState(() {
      _isVisible = !_isVisible;
    });
  }

  Widget build(BuildContext context) {
    var _height = MediaQuery.of(context).size.height;
    var _width = MediaQuery.of(context).size.width;

    return Container(
      color: Colors.white,
      child: Stack(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(
                    top: _height * 0.01,
                    bottom: _height * 0.01,
                    left: _width * 0.05),
                child: Text(
                  "League",
                  style: GoogleFonts.nunito(
                    fontWeight: FontWeight.w700,
                    textStyle: TextStyle(
                      color: Color.fromRGBO(142, 118, 176, 1),
                      // color: Color.fromRGBO(0, 3, 42, 1),
                      fontSize: 24,
                    ),
                  ),
                ),
              ),
              Row(
                children: <Widget>[
                  Container(
                    width: _width * 0.24,
                    height: _height * 0.042,
                    decoration: BoxDecoration(
                      color: _isVisible
                          ? Color.fromRGBO(195, 159, 202, 1)
                          : Colors.transparent,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        bottomLeft: Radius.circular(20),
                      ),
                      border: Border.all(
                        color: !_isVisible
                            ? Color.fromRGBO(195, 159, 202, 1)
                            : Colors.transparent,
                      ),
                    ),
                    child: Center(
                        child: MaterialButton(
                      child: Text(
                        "Week",
                        style: GoogleFonts.nunito(
                          fontWeight: FontWeight.w700,
                          textStyle: TextStyle(
                            color: _isVisible
                                ? Color.fromRGBO(255, 255, 255, 1)
                                : Color.fromRGBO(195, 159, 202, 1),
                          ),
                        ),
                      ),
                      onPressed: () => {_isVisible ? null : changeView()},
                    )),
                  ),
                  Container(
                    margin: EdgeInsets.only(right: _width * 0.04),
                    width: _width * 0.24,
                    height: _height * 0.042,
                    decoration: BoxDecoration(
                      color: !_isVisible
                          ? Color.fromRGBO(195, 159, 202, 1)
                          : Colors.transparent,
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(20),
                        bottomRight: Radius.circular(20),
                      ),
                      border: Border.all(
                        color: !_isVisible
                            ? Colors.transparent
                            : Color.fromRGBO(195, 159, 202, 1),
                      ),
                    ),
                    child: Center(
                        child: MaterialButton(
                      child: Text(
                        "General",
                        style: GoogleFonts.nunito(
                          fontWeight: FontWeight.w700,
                          textStyle: TextStyle(
                            color: !_isVisible
                                ? Color.fromRGBO(255, 255, 255, 1)
                                : Color.fromRGBO(195, 159, 202, 1),
                          ),
                        ),
                      ),
                      onPressed: () => {!_isVisible ? null : changeView()},
                    )),
                  ),
                ],
              )
            ],
          ),
          Container(
            margin: EdgeInsets.only(
              top: _height * 0.08,
            ),
            child: Visibility(
              child: ListView(
                children: <Widget>[
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          color: Colors.transparent,
                          height: _height * 0.26,
                          width: _width * 0.33,
                          child: Column(
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(top: _height * 0.03),
                                height: _height * 0.08,
                                width: _width * 0.16,
                                decoration: BoxDecoration(
                                    color: Color.fromRGBO(195, 159, 202, 1),
                                    shape: BoxShape.circle),
                              ),
                              Container(
                                height: _height * 0.05,
                                width: _width * 0.33,
                                decoration: BoxDecoration(
                                  color: Colors.transparent,
                                ),
                                child: Center(
                                  child: Text(
                                    "PLAYER NAME",
                                    style: GoogleFonts.nunito(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w700),
                                  ),
                                ),
                              ),
                              Container(
                                height: _height * 0.05,
                                width: _width * 0.33,
                                decoration: BoxDecoration(
                                  color: Colors.transparent,
                                ),
                                child: Center(
                                  child: Text(
                                    "0 PTS",
                                    style: GoogleFonts.nunito(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w700),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          color: Colors.transparent,
                          height: _height * 0.26,
                          width: _width * 0.33,
                          child: Column(
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(top: _height * 0.01),
                                height: _height * 0.08,
                                width: _width * 0.16,
                                decoration: BoxDecoration(
                                    color: Color.fromRGBO(195, 159, 202, 1),
                                    shape: BoxShape.circle),
                              ),
                              Container(
                                height: _height * 0.05,
                                width: _width * 0.33,
                                decoration: BoxDecoration(
                                  color: Colors.transparent,
                                ),
                                child: Center(
                                  child: Text(
                                    "PLAYER NAME",
                                    style: GoogleFonts.nunito(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w700),
                                  ),
                                ),
                              ),
                              Container(
                                height: _height * 0.05,
                                width: _width * 0.33,
                                decoration: BoxDecoration(
                                  color: Colors.transparent,
                                ),
                                child: Center(
                                  child: Text(
                                    "0 PTS",
                                    style: GoogleFonts.nunito(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w700),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          color: Colors.transparent,
                          height: _height * 0.26,
                          width: _width * 0.33,
                          child: Column(
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(top: _height * 0.04),
                                height: _height * 0.08,
                                width: _width * 0.16,
                                decoration: BoxDecoration(
                                    color: Color.fromRGBO(195, 159, 202, 1),
                                    shape: BoxShape.circle),
                              ),
                              Container(
                                height: _height * 0.05,
                                width: _width * 0.33,
                                decoration: BoxDecoration(
                                  color: Colors.transparent,
                                ),
                                child: Center(
                                  child: Text(
                                    "PLAYER NAME",
                                    style: GoogleFonts.nunito(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w700),
                                  ),
                                ),
                              ),
                              Container(
                                height: _height * 0.05,
                                width: _width * 0.33,
                                decoration: BoxDecoration(
                                  color: Colors.transparent,
                                ),
                                child: Center(
                                  child: Text(
                                    "0 PTS",
                                    style: GoogleFonts.nunito(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w700),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: _height * 0.6,
                    color: Colors.transparent,
                    child: ListView(
                      children: [
                        Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              leaderborderLine(context, "4", "10"),
                              leaderborderLine(context, "5", "10"),
                              leaderborderLine(context, "6", "10"),
                              leaderborderLine(context, "7", "10"),
                              leaderborderLine(context, "8", "10"),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              visible: _isVisible,
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              top: _height * 0.08,
            ),
            child: Visibility(
              child: ListView(
                children: <Widget>[
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          color: Colors.transparent,
                          height: _height * 0.26,
                          width: _width * 0.33,
                          child: Column(
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(top: _height * 0.03),
                                height: _height * 0.08,
                                width: _width * 0.16,
                                decoration: BoxDecoration(
                                    color: Color.fromRGBO(195, 159, 202, 1),
                                    shape: BoxShape.circle),
                              ),
                              Container(
                                height: _height * 0.05,
                                width: _width * 0.33,
                                decoration: BoxDecoration(
                                  color: Colors.transparent,
                                ),
                                child: Center(
                                  child: Text(
                                    "PLAYER NAME",
                                    style: GoogleFonts.nunito(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w700),
                                  ),
                                ),
                              ),
                              Container(
                                height: _height * 0.05,
                                width: _width * 0.33,
                                decoration: BoxDecoration(
                                  color: Colors.transparent,
                                ),
                                child: Center(
                                  child: Text(
                                    "0 PTS",
                                    style: GoogleFonts.nunito(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w700),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          color: Colors.transparent,
                          height: _height * 0.26,
                          width: _width * 0.33,
                          child: Column(
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(top: _height * 0.01),
                                height: _height * 0.08,
                                width: _width * 0.16,
                                decoration: BoxDecoration(
                                    color: Color.fromRGBO(195, 159, 202, 1),
                                    shape: BoxShape.circle),
                              ),
                              Container(
                                height: _height * 0.05,
                                width: _width * 0.33,
                                decoration: BoxDecoration(
                                  color: Colors.transparent,
                                ),
                                child: Center(
                                  child: Text(
                                    "PLAYER NAME",
                                    style: GoogleFonts.nunito(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w700),
                                  ),
                                ),
                              ),
                              Container(
                                height: _height * 0.05,
                                width: _width * 0.33,
                                decoration: BoxDecoration(
                                  color: Colors.transparent,
                                ),
                                child: Center(
                                  child: Text(
                                    "0 PTS",
                                    style: GoogleFonts.nunito(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w700),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          color: Colors.transparent,
                          height: _height * 0.26,
                          width: _width * 0.33,
                          child: Column(
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(top: _height * 0.04),
                                height: _height * 0.08,
                                width: _width * 0.16,
                                decoration: BoxDecoration(
                                    color: Color.fromRGBO(195, 159, 202, 1),
                                    shape: BoxShape.circle),
                              ),
                              Container(
                                height: _height * 0.05,
                                width: _width * 0.33,
                                decoration: BoxDecoration(
                                  color: Colors.transparent,
                                ),
                                child: Center(
                                  child: Text(
                                    "PLAYER NAME",
                                    style: GoogleFonts.nunito(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w700),
                                  ),
                                ),
                              ),
                              Container(
                                height: _height * 0.05,
                                width: _width * 0.33,
                                decoration: BoxDecoration(
                                  color: Colors.transparent,
                                ),
                                child: Center(
                                  child: Text(
                                    "0 PTS",
                                    style: GoogleFonts.nunito(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w700),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: _height * 0.6,
                    color: Colors.transparent,
                    child: ListView(
                      children: [
                        Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              leaderborderLine(context, "4", "10"),
                              leaderborderLine(context, "5", "10"),
                              leaderborderLine(context, "6", "10"),
                              leaderborderLine(context, "7", "10"),
                              leaderborderLine(context, "8", "10"),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              visible: !_isVisible,
            ),
          )
        ],
      ),
    );
  }
}

Widget leaderborderLine(context, pos, pts) {
  var _height = MediaQuery.of(context).size.height;
  var _width = MediaQuery.of(context).size.width;
  return Container(
    height: _height * 0.08,
    width: _width * 1,
    decoration: BoxDecoration(
      color: Colors.transparent,
      border: Border(
        bottom:
            BorderSide(color: Color.fromRGBO(195, 159, 202, 0.5), width: 0.8),
      ),
    ),
    child: Row(
      children: <Widget>[
        Container(
          width: _width * 0.12,
          height: _height * 0.06,
          color: Colors.transparent,
          child: Center(
            child: Text(
              pos,
              style: GoogleFonts.nunito(
                fontSize: 24,
                fontWeight: FontWeight.w700,
                color: Color.fromRGBO(195, 159, 202, 1),
              ),
            ),
          ),
        ),
        Container(
          width: _width * 0.10,
          height: _height * 0.06,
          decoration: BoxDecoration(
              color: Color.fromRGBO(195, 159, 202, 1), shape: BoxShape.circle),
        ),
        Container(
          width: _width * 0.55,
          height: _height * 0.06,
          decoration: BoxDecoration(
            color: Colors.transparent,
          ),
          child: Center(
            child: Text(
              "Player Name".toUpperCase(),
              style:
                  GoogleFonts.nunito(fontSize: 14, fontWeight: FontWeight.w700),
            ),
          ),
        ),
        Container(
          width: _width * 0.2,
          height: _height * 0.06,
          decoration: BoxDecoration(
            color: Colors.transparent,
          ),
          child: Center(
            child: Text(
              pts + " PTS",
              style:
                  GoogleFonts.nunito(fontSize: 16, fontWeight: FontWeight.w700),
            ),
          ),
        ),
      ],
    ),
  );
}
