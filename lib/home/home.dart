import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  Widget build(BuildContext context) {
    var _height = MediaQuery.of(context).size.height;
    var _width = MediaQuery.of(context).size.width;
    return Container(
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          // TITLE
          Container(
            margin: EdgeInsets.only(
              top: _height * 0.01,
              bottom: _height * 0.02,
              left: _width * 0.05,
            ),
            child: Text(
              "Home",
              style: GoogleFonts.nunito(
                fontWeight: FontWeight.w700,
                textStyle: TextStyle(
                  color: Color.fromRGBO(142, 118, 176, 1),
                  fontSize: 24,
                ),
              ),
            ),
          ),

          // Week Calendar
        ],
      ),
    );
  }
}

Widget weekMatch(context) {
  var _height = MediaQuery.of(context).size.height;
  var _width = MediaQuery.of(context).size.width;
  return Container(
    margin: EdgeInsets.only(bottom: _height * 0.01),
    height: _height * 0.064,
    width: _width * 0.4,
    decoration: BoxDecoration(
      color: Color.fromRGBO(98, 81, 202, 0.5),
      // color: Color.fromRGBO(255, 255, 255, 0.6),
      borderRadius: BorderRadius.circular(20),
    ),
    child: Column(
      children: [
        Container(
          margin: EdgeInsets.only(top: _height * 0.005),
          child: Text(
            "01-01-21",
            style: GoogleFonts.nunito(
              fontSize: 10,
              fontWeight: FontWeight.w700,
              textStyle: TextStyle(
                color: Color.fromRGBO(255, 255, 255, 1),
              ),
            ),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Container(
              height: _height * 0.035,
              width: _width * 0.07,
              decoration: BoxDecoration(
                color: Colors.red,
                borderRadius: BorderRadius.circular(6),
                image: DecorationImage(
                  image: AssetImage('assets/images/icon.png'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Container(
              child: Text(
                "19:00",
                style: GoogleFonts.nunito(
                  fontWeight: FontWeight.w700,
                  textStyle: TextStyle(
                    color: Color.fromRGBO(255, 255, 255, 1),
                  ),
                ),
              ),
            ),
            Container(
              height: _height * 0.035,
              width: _width * 0.07,
              decoration: BoxDecoration(
                color: Colors.red,
                borderRadius: BorderRadius.circular(6),
                image: DecorationImage(
                  image: AssetImage('assets/images/icon.png'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ],
        )
      ],
    ),
  );
}
