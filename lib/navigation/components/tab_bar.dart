import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyTabBar extends StatefulWidget {
  @override
  _TabBarState createState() => _TabBarState();
}

class _TabBarState extends State<MyTabBar> {
  Widget build(BuildContext context) {
    return Container(
      height: 80,
      color: Colors.white,
      decoration: null,
      child: TabBar(
        indicatorColor: Colors.transparent,
        labelColor: Color(0XFF490457),
        unselectedLabelColor: Color(0xFFc39fca),
        tabs: [
          Tab(
            icon: Icon(
              Icons.home_rounded,
              size: 36,
            ),
          ),
          Tab(
            icon: Icon(
              Icons.sports_hockey_rounded,
              size: 36,
            ),
          ),
          Tab(
            icon: Icon(
              Icons.compare_arrows_rounded,
              size: 36,
            ),
          ),
          Tab(
            icon: Icon(
              Icons.emoji_events_rounded,
              size: 36,
            ),
          ),
        ],
      ),
    );
  }
}
