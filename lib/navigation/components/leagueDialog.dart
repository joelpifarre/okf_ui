import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:okfantasy_app/api/api.dart';
import 'package:okfantasy_app/league/league.dart';
import 'package:okfantasy_app/models/leagues.dart';

showMyLeaguesList(context) {
  var _height = MediaQuery.of(context).size.height;

  Dialog leagueList = Dialog(
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(20),
    ),
    child: Container(
      height: _height * 0.5,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
      ),
      child: MyListScreen(),
    ),
  );

  showDialog(
      context: context,
      builder: (BuildContext context) {
        return leagueList;
      });
}

class MyListScreen extends StatefulWidget {
  @override
  createState() => _MyListScreenState();
}

class _MyListScreenState extends State {
  var leagues = new List<LeagueClass>();

  _getUserLeagues() {
    API().getUserLeagues().then((response) {
      setState(() {
        Iterable list = response;
        leagues = list.map((model) => LeagueClass.fromJson(model)).toList();
      });
    });
  }

  initState() {
    super.initState();
    _getUserLeagues();
  }

  dispose() {
    super.dispose();
  }

  @override
  build(context) {
    double _width = MediaQuery.of(context).size.width;
    double _height = MediaQuery.of(context).size.height;
    return ListView.builder(
      itemCount: leagues.length,
      itemBuilder: (context, index) {
        return Container(
          height: _height * 0.06,
          width: _width * 0.8,
          margin: EdgeInsets.only(
            left: _width * 0.1,
            right: _width * 0.1,
          ),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    leagues[index].name,
                    style: GoogleFonts.nunito(fontSize: 16),
                  ),
                  Text(leagues[index].realLeague),
                ],
              )
            ],
          ),
        );
      },
    );
  }
}
