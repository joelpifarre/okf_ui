import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TopBar extends StatefulWidget {
  @override
  _TopBarState createState() => _TopBarState();
}

class _TopBarState extends State<TopBar> {
  Widget build(BuildContext context) {
    return  AppBar(
    leading: ButtonBar(
      children: [
        Center(
          child: FlatButton(
            child: CircleAvatar(
            backgroundColor: Colors.black,
          ),
          color: Colors.green,
          onPressed: ()=>{
          
          },
        ),
          )
      ],
    ),
    iconTheme: IconThemeData(
      color: Colors.black, //change your color here
    ),
    backgroundColor: Colors.white,
    centerTitle: true,
    title: Text('League Name'),
    elevation: 0,
  );
  }
}
