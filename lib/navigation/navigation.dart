import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:okfantasy_app/home/home.dart';
import 'package:okfantasy_app/league/league.dart';
import 'package:okfantasy_app/market/market.dart';
import 'package:okfantasy_app/navigation/components/leagueDialog.dart';
import 'package:okfantasy_app/navigation/components/tab_bar.dart';
import 'package:okfantasy_app/roster/roster.dart';

class Navigation extends StatefulWidget {
  @override
  _NavigationState createState() => _NavigationState();
}

class _NavigationState extends State<Navigation> {
  Widget build(BuildContext context) {
    var _height = MediaQuery.of(context).size.height;
    var _width = MediaQuery.of(context).size.width;
    return DefaultTabController(
        length: 4,
        initialIndex: 0,
        child: Scaffold(
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.white,
            centerTitle: true,
            title: MaterialButton(
              child: Text(
                "League Name",
                style: GoogleFonts.nunito(
                    fontSize: 20,
                    textStyle: TextStyle(
                      color: Color.fromRGBO(142, 118, 176, 1),
                    ),
                    fontWeight: FontWeight.w700),
              ),
              onPressed: () => {showMyLeaguesList(context)},
            ),
            leading: ButtonBar(
              children: <Widget>[
                MaterialButton(
                  splashColor: Colors.transparent,
                  onPressed: () => {},
                  child: Icon(
                    Icons.notes_rounded,
                    size: 26,
                    color: Color.fromRGBO(142, 118, 176, 1),
                  ),
                )
              ],
            ),
            actions: [
              ButtonBar(
                children: <Widget>[
                  MaterialButton(
                    onPressed: () => {},
                    child: Icon(
                      Icons.notifications_outlined,
                      size: 26,
                      color: Color.fromRGBO(142, 118, 176, 1),
                    ),
                  )
                ],
              ),
            ],
          ),
          body: TabBarView(
            children: [
              Home(),
              Roster(),
              Market(),
              League(),
            ],
          ),
          bottomNavigationBar: MyTabBar(),
        ));
  }
}
