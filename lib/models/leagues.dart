class LeagueClass {
  int id = 0;
  String name = "";
  String realLeague = "";
  bool isPublic = true;
  String maxPlayers = "";
  String initMoney = "";
  String code = "";
  String maxTranf = "";

  LeagueClass(int id, String name, bool isPublic, String realLeague,
      String maxPlayers, String initMoney, String code, String maxTranf) {
    this.id = id;
    this.name = name;
    this.isPublic = isPublic;
    this.realLeague = realLeague;
    this.maxPlayers = maxPlayers;
    this.initMoney = initMoney;
    this.code = code;
    this.maxTranf = maxTranf;
  }

  LeagueClass.fromJson(Map json)
      : id = json['id'],
        name = json['name'],
        isPublic = json['isPublic'],
        realLeague = json['real_league'],
        maxPlayers = json['max_players'],
        initMoney = json['init_money'],
        code = json['code'],
        maxTranf = json['max_tranf_player'];

  // Map toJson() {
  //   return {'id': id, 'name': name, 'lastname': realLeague};
  // }
}
