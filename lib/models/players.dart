class RPlayerClass {
  int id = 0;
  String name = "";
  String lastname = "";
  String pos = "";
  String logo = "";
  String image = "";
  String initPos = "";

  RPlayerClass(int id, String name, String lastname, String pos, String logo,
      String image, String initPos) {
    this.id = id;
    this.name = name;
    this.lastname = lastname;
    this.pos = pos;
    this.logo = logo;
    this.image = image;
    this.initPos = initPos;
  }

  RPlayerClass.fromJson(Map json)
      : id = json['id'],
        name = json['name'],
        lastname = json['lastname'],
        pos = json['position'],
        logo = json['logo'],
        image = json['image'],
        initPos = json['initPos'];

  Map toJson() {
    return {
      'id': id,
      'name': name,
      'lastname': lastname,
      'position': pos,
      'logo': logo
    };
  }
}
